(ns hatchet.core
  (:require [clojure.string :as str]
            [hatchet.git :as git]
            [hatchet.logger :as log])
  (:gen-class))

(defn transform-detail-list->details-maps [[n & changes]]
  (let [name (str/replace n #"\"" "")]
    (pmap (fn [ch] {:name name :detail ch})
          changes)))

(defn union-re-patterns [& patterns]
  (re-pattern (str/join "|" patterns)))

(defn blacklisted [{detail :detail}]
  (let [pattern (union-re-patterns "\\.mp4$" "\\.png$" "\\.enc$" "\\.pdf$" "\\.dat$" "\\.ico$"
                                   "\\.gif$" "\\.jpe?g$" "\\.DS_Store$" "gz$" "\\.gem$" "\\.dump$"
                                   "\\.swp$" "\\.sqlite3" "binary_file$" "mixed_files$" "raw_base64_decoded_string$" "img$")]
    (re-find pattern detail)))

(defn detail->changeset [{name :name detail :detail}]
  (try
    (let [[added removed file] (str/split detail #"\t")]
      (log/counted-file file)
      {:name name :added (Integer/parseInt added) :removed (Integer/parseInt removed)})
    (catch Exception e
      (log/problem-file detail)
      {:name name :added 0 :removed 0})))

(defn merge-changeset [{a1 :added r1 :removed} {a2 :added r2 :removed}]
  {:added (+ a1 a2) :removed (+ r1 r2)})

(defn merge-changeset-group [[n cs]]
  (merge {:name n} (reduce merge-changeset cs)))

(defn hatchet-data [repo-path]
  (->> repo-path git/commit-shas
       (pmap #(git/commit-details repo-path %))
       (mapcat transform-detail-list->details-maps)
       (remove blacklisted)
       (pmap detail->changeset)
       (group-by :name)
       (pmap merge-changeset-group)))

(defn -main
  "I tally the blood from the repo located at `p`."
  [p]
  (log/delete-logs)
  (dorun (map log/score (hatchet-data p)))
  (log/dedupe))
