(ns hatchet.logger
  (:require [clojure.java.io :as io])
  (:gen-class))

(defn file-path [n]
  (str (System/getProperty "user.home") "/.hatchet/" n))

(defn log [n m]
  (spit (file-path n) (str m "\n") :append true))

(defn problem-file [f]
  (log "problem-file.log" f))

(defn counted-file [f]
  (log "counted-file.log" f))

(defn score [{name :name added :added removed :removed }]
  (log "scores.log" (format "%8d\t%8d%8d\t%s" added removed (- added removed) name)))


(defn delete-logs []
  (io/delete-file (file-path "problem-file.log"))
  (io/delete-file (file-path "counted-file.log"))
  (io/delete-file (file-path "scores.log")))

(defn dedupe []
  (dorun (map (fn [f]
                (let [c (slurp (file-path f))]
                  (io/delete-file (file-path f))
                  (dorun (map #(log f %) (sort (distinct (clojure.string/split c #"\n")))))))
              #{"counted-file.log" "problem-file.log"})))
