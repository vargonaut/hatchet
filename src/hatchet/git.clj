(ns hatchet.git
  (:require  [clojure.string :as str]
             [clojure.java.shell :as shell :only [sh with-sh-dir]])
  (:gen-class))

(defn exec [dir command]
  (:out (apply shell/sh
               (concat (cons "/usr/bin/git" (str/split command #" "))
                       [:dir dir]))))

;;; ******************************************************************

(defn commit-shas-raw [dir]
  (exec dir "rev-list --no-merges HEAD"))

(defn commit-details-raw [dir sha]
  (exec dir (str "show --numstat --oneline --pretty=format:\"%an\" " sha)))

;;; ******************************************************************

(defn commit-shas [dir]
  (str/split-lines (commit-shas-raw dir)))

(defn commit-details [dir sha]
  (str/split-lines (commit-details-raw dir sha)))
