# hatchet

FIXME: description

## Installation

Download from http://example.com/FIXME.

## Usage

FIXME: explanation

    $ java -jar hatchet-0.1.0-standalone.jar path/to/git/repository

FIXME: listing of options this app accepts.

## Data Stuctures

Walking the chain

### get commit list

Every commit in the repository

    ("sha1" "sha2" "sha3" "...")

### get commit details, split by line

The adds and removes of a given commit. Binaries have "-" instead of a number for added & removed.

After name, lines are `added removed file-name`, tab separated.

    ["\"Andrew Vargo\""
     "1\t1\tsrc/game_of_life/core.clj"
     "1\t1\tsrc/game_of_life/display.clj"
     "-\t-\tsrc/game_of_life/ex.jpg"]

### transform details vector in map per file

Commit details processed and broken into maps that have a name and
details. All the details in the repository will be combined into a
single vector. Its not worth splitting details now b/c we will remove
blacklisted first.

    [{:name "Andrew Vargo" :detail "60\t1\tsrc/game_of_life/core.clj"}
     {:name "Andrew Vargo" :detail "27\t0\tsrc/game_of_life/io.clj"}
     {:name "Andrew Vargo" :detail "-\t-\tsrc/game_of_life/ex.jpg"}]

### filter out blacklisted

`:detail` will be matched against a list of regular expressions in a
black list. Anything that matches is dropped.

    [{:name "Andrew Vargo" :detail "60\t1\tsrc/game_of_life/core.clj"}
     {:name "Andrew Vargo" :detail "27\t0\tsrc/game_of_life/io.clj"}]

### transform detail maps to changesets

We can now split the details up. The numbers have to be parsed. The
details are logged if parsing fails and a zero'd map is used. If
parsing does work, the file gets logged as kept.

    [{:name "Andrew Vargo" :added 60 :removed 1}
     {:name "Andrew Vargo" :added 27 :removed 0}]

### group by name

The count maps are grouped by name.

    {"Andrew Vargo" [{:name "Andrew Vargo" :added 60 :removed 1}
                     {:name "Andrew Vargo" :added 27 :removed 0}]}

### reduce to totals

Each group is reduced by summing `added` and `removed`. Aside from
sorting, you're set.

    {:name "Andrew Vargo" :added 87 :removed 1}

## Logging

Three types of logging happen. A folder `$HOME/.hatchet` to put the logs
is assumed. Using the `-main_` function will delete the logs and then provide some terrible de-duplication.

1. `problem-file.log` captures details that couldn't be parsed. These
   are good candidates to add to the black list (or adjust parsing.)
2. `counted-file.log` captures files that have counted changes. These
   are saved so you can argue over things that should and shouldn't be
   included in the hatchet scores.
3. `scores.log` will have the hatchet scores pretty printed.

## License

Copyright © 2017 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
